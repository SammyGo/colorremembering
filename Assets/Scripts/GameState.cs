namespace ColorRemembering
{
    public abstract class GameState
	{
		protected StateMachine GameManager;

		protected GameState(StateMachine gameManager)
		{
			GameManager = gameManager;
		}

		public virtual void Enter() { }
		public virtual void Update() { }
		public virtual void Leave() { }
	}
}
