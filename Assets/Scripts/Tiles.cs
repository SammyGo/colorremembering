using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorRemembering
{
    public class Tiles : MonoBehaviour
    {
        Material _material;
        bool _isEnabled;
        float _time;
        float _runtime;    
    
        void Start()
        {
            this._material = GetComponent<Renderer>().material;
            this._material.DisableKeyword("_EMISSION");
            this._isEnabled = false;
            this._time = 0;
        }

        void Update()
        {
            this._runtime += Time.deltaTime;
            if (this._runtime >= this._time + 1)
            {
                this._time = this._runtime;
                this.ToggleEmission();
            }
        }

        void ToggleEmission()
        {
            if (_isEnabled)
            {
                this._material.DisableKeyword("_EMISSION");
                this._isEnabled = false;
            }
            else
            {
                this._material.EnableKeyword("_EMISSION");
                this._isEnabled = true;
            }
        }
    }
}