using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorRemembering
{
    public class WrongPattern : GameState
    {
        public WrongPattern(StateMachine stateMachine) : base(stateMachine) { }
    }
}
