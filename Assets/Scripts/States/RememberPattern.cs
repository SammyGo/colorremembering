using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorRemembering
{
    public class RememberPattern : GameState
    {
        private bool _isWrong = true;
        
        public RememberPattern(StateMachine stateMachine) : base(stateMachine) { }

        public override void Update()
        {
            if (_isWrong)
            {
                GameManager.SetState(new WrongPattern(GameManager));
            }
            else
            {
                GameManager.SetState(new RightPattern(GameManager));
            }
        }
    }
}

