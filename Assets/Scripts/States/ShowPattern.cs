using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorRemembering
{
    public class ShowPattern : GameState
    {
        float _timer = 0f;

        public ShowPattern(StateMachine gameManager) : base(gameManager) { }

        public override void Update()
        {
            this._timer += Time.deltaTime;

            if (this._timer >= 5f)
            {
                this.GameManager.SetState(new RememberPattern(this.GameManager));
            }
        }
    }
}