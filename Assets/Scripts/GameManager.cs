using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorRemembering
{
    public class GameManager : StateMachine
    {
        private void Start()
        {
            this.SetState(new ShowPattern(this));
        }
    }
}

