using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColorRemembering
{
    public abstract class StateMachine : MonoBehaviour
    {
        protected GameState State;

        public void SetState(GameState state)
        {
            if (this.State != null)
            {
                Debug.Log($"Leaving state '{this.State.GetType()}'");
                this.State.Leave();
            }
            else
            {
                Debug.Log("Starting from blank.");
            }

            this.State = state;
            this.State.Enter();
            Debug.Log($"Entering state '{this.State.GetType()}'");
        }

        void Update()
        {
            this.State.Update();
        }
    }  
}

